﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    public class Player : Creature
    {
        public int MonsterKills { get; set; }
        public int PlayerDeath { get; set; }
        public int RoomNumber { get; set; }
        public Player()
        {
            Life = 5;
            Damage = 1;
            TreasureCount = 0;
            MonsterKills = 0;
            PlayerDeath = 0;
            RoomNumber = 0;
        }

        public Player(int life, int damage, int treasureCount, int monsterKills, int playerDeath)
        {
            Life = life;
            Damage = damage;
            TreasureCount = treasureCount;
            MonsterKills = monsterKills;
            PlayerDeath = playerDeath;
        }
    }
}
