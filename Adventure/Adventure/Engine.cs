﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    public class Engine
    {
        public int DealDamage(Creature damageDealer, Creature damageTaker)
        {
            damageTaker.Life = (damageTaker.Life - damageDealer.Damage);
            if (damageTaker.Life < 1)
            {
                if (damageTaker is Monster)
                {
                    var player = (Player)damageDealer;
                    MonsterKill(player);
                }
                if (damageTaker is Player)
                {
                    var player = (Player)damageTaker;
                    PlayerKill(player);
                }
            }
            if (damageTaker.Life > 0)
            {
                //Damage dealer and damage taker are passed back swapped to change turns
                DealDamage(damageTaker, damageDealer);
            }
            return damageTaker.Life;
        }

        public void PlayerKill(Player player)
        {
            player.PlayerDeath += 1;
        }

        public void MonsterKill(Player player)
        {
            AwardTreasure(player);
            player.MonsterKills += 1;
        }

        public void AwardTreasure(Player player)
        {
            var rnd = new Random();
            player.TreasureCount += rnd.Next(0, 25);
        }

        public int Heal(Creature creatureBeingHealed, Room newRoom)
        {
            creatureBeingHealed.Life += newRoom.HealFactor;
            return creatureBeingHealed.Life;
        }

        public int PickUpTreasure(Room newRoom, Player newPlayer)
        {
            newPlayer.TreasureCount = +newRoom.TreasureAmount;
            return newPlayer.TreasureCount;
        }

        public int MoveToNextRoom(Player newPlayer)
        {
            newPlayer.RoomNumber += 1;
            return newPlayer.RoomNumber;
        }

        public Monster SpawnMonster()
        {
            Monster newMonster = new Monster(1, 1);
            return newMonster;
        }

        public void FillRoom(Room newRoom)
        {
            // random 1 - 10, 1, 4, 7 = monster
            // random 0 - 50 = treasure amount
            Random rnd = new Random();

            var monsterSeed = rnd.Next(1, 10);
            if (monsterSeed == 1 || monsterSeed == 4 || monsterSeed == 7)
            {
                newRoom.ContainsMonster = true;
            }
            newRoom.TreasureAmount = rnd.Next(0, 50);
        }

        public void GetRoomContents(Room newRoom, Player newPlayer)
        {
            if (newRoom.ContainsMonster)
            {
                var newMonster = SpawnMonster();
                Battle(newPlayer, newMonster);
            }
        }

        //public bool GetTurn()
        //{
        //    //function to tell if monster or player turn
        //    return true;
        //}

        public void Battle(Creature damageDealer, Creature damageTaker)
        {
            DealDamage(damageDealer, damageTaker);
        }
    }

    //public int TakeDamagePlayer(Monster monster, Player player)
    //{
    //    player.Life -= monster.Damage;
    //    return player.Life;
    //}


    //public int TakeDamageMonster(Monster monster, Player player)
    //{
    //    monster.Life -= player.Damage;
    //    return monster.Life;
    //}

}

