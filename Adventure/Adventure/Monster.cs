﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    public class Monster : Creature
    {
        public Monster()
        {
            Life = 5;
            Damage = 1;
        }

        public Monster(int life, int damage)
        {
            Life = life;
            Damage = damage;
        }
    }
}
