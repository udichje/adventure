﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    public class Room : Area
    {
        public Room()
        {
            DamageFactor = 1;
            HealFactor = 1;
            TreasureAmount = 0;
            AreaNumber = 1;
            RoomNumber = 1;
            ContainsMonster = false;
        }
    }
}
