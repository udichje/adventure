﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    public class Area
    {
        public int DamageFactor { get; set; }
        public int HealFactor { get; set; }
        public int TreasureAmount { get; set; }
        //public bool ContainsTreasure { get; set; }
        public bool ContainsMonster { get; set; }
        public int AreaNumber { get; set; }
        public int RoomNumber { get; set; }

    }
}
