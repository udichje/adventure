﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure
{
    public class Creature
    {
        public int Life { get; set; }
        public int Damage { get; set; }
        public int TreasureCount { get; set; }
    }
}
