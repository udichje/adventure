﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
//using Adventure;

namespace Adventure.Test
{
    [TestFixture]
    class AdventureTestMethods
    {
        Player newPlayer;
        Player hurtPlayer;
        Monster newMonster;
        Monster hurtMonster;
        Engine controlEngine;
        Room newRoom;

        [SetUp]
        public void Init()
        {
            newPlayer = new Player();
            hurtPlayer = new Player(1, 1, 0, 0, 0);
            newMonster = new Monster();
            hurtMonster = new Monster(1, 1);
            controlEngine = new Engine();
            newRoom = new Room();
        }

        //[Test]
        //public void TestPlayerDamage()
        //{

        //    var expected = newPlayer.Life - 1;
        //    var actual = controlEngine.TakeDamagePlayer(newMonster, newPlayer);
        //    Assert.AreEqual(expected, actual, "should be 4");
        //}


        //[Test]
        //public void TestMonsterDamage()
        //{
        //    var expected = newMonster.Life - 1;
        //    var actual = controlEngine.TakeDamageMonster(newMonster, newPlayer);
        //    Assert.AreEqual(expected, actual, "should be 4");
        //}

        [Test]
        public void PlayerTakeDamage()
        {
            var expected = 0; // loop will run until player is dead
            var actual = controlEngine.DealDamage(newMonster, newPlayer);
            Assert.AreEqual(expected, actual, "Player life [ 5 ]  should be decreased by Monster damage" + newMonster.Damage);
        }

        [Test]
        public void MonsterTakeDamage()
        {
            var expected = 0; // loop will run until player is dead
            var actual = controlEngine.DealDamage(newPlayer, newMonster);
            Assert.AreEqual(expected, actual, "Monster life [ 5 ]  should be decreased by player damage" + newPlayer.Damage);
        }

        [Test]
        public void TestHeal()
        {
            var expected = (newPlayer.Life + newRoom.HealFactor);
            var actual = controlEngine.Heal(newPlayer, newRoom);
            Assert.AreEqual(expected, actual, "Player life [ 4 ] should now be [ 5 ]");
        }

        [Test]
        public void PickUpTreasure()
        {
            var expected = (newPlayer.TreasureCount + newRoom.TreasureAmount);
            var actual = controlEngine.PickUpTreasure(newRoom, newPlayer);
            Assert.AreEqual(expected, actual, "Player treasure [ 0 ] should be increased by room treasure [{0}]", newRoom.TreasureAmount);
        }

        [Test]
        public void KillMonster()
        {
            var expected = (newPlayer.MonsterKills + 1);
            controlEngine.DealDamage(newPlayer, hurtMonster);
            var actual = newPlayer.MonsterKills;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void KillPlayer()
        {
            var expected = (hurtPlayer.PlayerDeath + 1);
            controlEngine.DealDamage(newMonster, hurtPlayer);
            var actual = hurtPlayer.PlayerDeath;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void MoveToNextRoom()
        {
            var expected = (newPlayer.RoomNumber + 1);
            var actual = controlEngine.MoveToNextRoom(newPlayer);
            Assert.AreEqual(expected, actual, "Room number should be 1");
        }

        [Test]
        public void FightMonsterInRoom()
        {
            var expected = (newPlayer.MonsterKills + 1);
            controlEngine.FillRoom(newRoom);
            controlEngine.GetRoomContents(newRoom, newPlayer);
            var actual = newPlayer.MonsterKills;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BattleMonsterDeath()
        {
            var expected = 1; // battle should result in player dying and monster kill increment
            controlEngine.Battle(newPlayer, newMonster);
            var actual = newPlayer.MonsterKills;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BattlePlayerDeath()
        {
            var expected = 1; // battle should result in player dying and player death increment
            controlEngine.Battle(newMonster, newPlayer);
            var actual = newPlayer.PlayerDeath;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetRoomContents()
        {
            var expected = 1; // room contains monster, battle should trigger with monster being killed and player monster kill incrementing
            controlEngine.FillRoom(newRoom);
            controlEngine.GetRoomContents(newRoom, newPlayer);
            var actual = newPlayer.MonsterKills;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [Ignore]
        public void FillRoomMonster()
        {
            //newRoom.ContainsMonster = false;
            controlEngine.FillRoom(newRoom);
            var actual = newRoom.ContainsMonster;
            Assert.True(actual);
        }

        [Test]
        [Ignore]
        public void FillRoomNoMonster()
        {
            controlEngine.FillRoom(newRoom);
            var actual = newRoom.ContainsMonster;
            Assert.False(actual);
        }

        [Test]
        [Ignore]
        public void AwardTreasureMonsterKill()
        {
            var expected = newPlayer.TreasureCount + 25;
            controlEngine.Battle(newPlayer, hurtMonster);
            var actual = newPlayer.TreasureCount;
            Assert.AreEqual(expected, actual);

        }


    }
}
